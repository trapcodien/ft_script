/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_script.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/01 07:58:39 by garm              #+#    #+#             */
/*   Updated: 2014/10/10 20:50:46 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SCRIPT_H
# define FT_SCRIPT_H

# include <sys/ioctl.h>
# include <sys/wait.h>
# include <sys/stat.h>
# include <sys/select.h>
# include <sys/time.h>
# include <time.h>
# include <limits.h>
# include <termios.h>
# include <fcntl.h>
# include <errno.h>

# include "libft.h"
# include "pty.h"

# define FT_SCRIPT_USAGE "usage: script [-akq] [-t time] [file [command ...]]"

# define FT_SUCCESS 0
# define FT_FAILURE 1

# define APPEND_OR_TRUNC(a) ((a) ? (O_APPEND) : (O_TRUNC))
# define GET_SCRIPT_OPEN_FLAG(a) (O_WRONLY | O_CREAT | APPEND_OR_TRUNC(a))

typedef struct termios	t_termios;
typedef struct winsize	t_winsize;

/*
** main.c
*/
int				get_pty(t_pty *pty);
void			init_term(t_termios *old_conf, t_termios *new_conf, int master);
void			process(t_pty *pty, int file, char **env);

/*
** time.c
*/
time_t			get_ts(void);
char			*get_date(void);

/*
** error.c
*/
void			ft_usage(void);
int				illegal_option(char *prog_name, char c);
int				permission_denied(char *prog_name, char *file);
int				invalid_flush_time(char *prog_name, int time);
int				pty_error(char *prog_name);

/*
** core.c
*/
void			ft_script(t_pty *pty, char **env, int file);
int				write_user_input(t_pty *pty, int master, int file);
int				write_shell_output(t_pty *pty, int master, int file);
int				watch_pty(t_pty *pty, int master, fd_set *fds);

/*
** flush.c
*/
int				flush_ibuffer(t_pty *pty, char **buf_ptr, char *buf, int *i);
int				flush_obuffer(t_pty *pty, char **buf_ptr, char *buf, int *i);
int				try_flush_input(t_pty *pty, char **buf_ptr, char *buf, int *i);
int				try_flush_output(t_pty *pty, char **buf_ptr, char *buf, int *i);

/*
** shell.c
*/
void			exec_shell(t_pty *pty, char **env, int file);

/*
** pty.c
*/
int				open_ptm(int flags);
int				grant_pts(int fdm);
int				unlock_pts(int fdm);
char			*get_ptsname(int fdm);
int				open_pts(const char *path, int flags);

/*
** parser.c
*/
int				ft_script_parser(int argc, char **argv, char **env, t_pty *pty);

#endif
