/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pty.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/03 05:31:56 by garm              #+#    #+#             */
/*   Updated: 2014/10/10 13:51:14 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PTY_H
# define PTY_H

typedef struct			s_pty
{
	char				a;
	char				k;
	char				q;
	int					time;
	char				flush;
	char				*file;
	int					fdfile;
	char				*cmd;
	char				**argv;
	int					nb_args;
	char				*prog_name;
	char				*shell;
	int					master;
	int					slave;
	time_t				last_flush;
}						t_pty;

#endif
