/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pty.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/01 07:59:32 by garm              #+#    #+#             */
/*   Updated: 2014/10/10 11:24:36 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_script.h"

int					open_ptm(int flags)
{
	int				fd;

	fd = open("/dev/ptmx", flags);
	if (fd >= 0)
		return (fd);
	return (ERROR);
}

int					grant_pts(int fdm)
{
	int				error;

	error = ioctl(fdm, TIOCPTYGRANT);
	if (error != 0)
		error = ERROR;
	return (error);
}

int					unlock_pts(int fdm)
{
	int				error;

	error = ioctl(fdm, TIOCPTYUNLK);
	if (error != 0)
		error = ERROR;
	return (error);
}

char				*get_ptsname(int fdm)
{
	static char		buffer[128];
	int				error;
	struct stat		infos_stat;
	char			*ptr;

	ptr = NULL;
	error = ioctl(fdm, TIOCPTYGNAME, buffer);
	if (!error && (stat(buffer, &infos_stat) == 0))
		ptr = buffer;
	return (ptr);
}

int					open_pts(const char *path, int flags)
{
	int				fd;

	fd = open(path, flags);
	if (fd >= 0)
		return (fd);
	return (ERROR);
}
