/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/08 23:50:47 by garm              #+#    #+#             */
/*   Updated: 2014/10/10 11:18:20 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_script.h"

void	ft_usage(void)
{
	ft_putendl_fd(FT_SCRIPT_USAGE, STDERR_FILENO);
}

int		illegal_option(char *prog_name, char c)
{
	ft_fprintf(STDERR_FILENO, "%s: Illegal option -- %c\n", prog_name, c);
	ft_usage();
	return (1);
}

int		permission_denied(char *prog_name, char *file)
{
	ft_fprintf(STDERR_FILENO, "%s: %s: Permission denied\n", prog_name, file);
	return (1);
}

int		invalid_flush_time(char *prog_name, int time)
{
	ft_fprintf(STDERR_FILENO, "%s: Invalid flush time %i\n", prog_name, time);
	return (1);
}

int		pty_error(char *prog_name)
{
	ft_fprintf(STDERR_FILENO, "%s: Unable to open tty\n", prog_name);
	return (1);
}
