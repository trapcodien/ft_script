/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shell.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/08 21:41:16 by garm              #+#    #+#             */
/*   Updated: 2014/10/10 11:24:39 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_script.h"

static void		init_shell_pty(t_pty *pty, int file)
{
	close(pty->master);
	close(file);
	setsid();
	ioctl(pty->slave, TIOCSCTTY, (char *)NULL);
	dup2(pty->slave, STDIN_FILENO);
	dup2(pty->slave, STDOUT_FILENO);
	dup2(pty->slave, STDERR_FILENO);
}

void			exec_shell(t_pty *pty, char **env, int file)
{
	char		**argv;
	char		*argv_interactive_mode[3];

	argv_interactive_mode[0] = pty->shell;
	argv_interactive_mode[1] = "-i";
	argv_interactive_mode[2] = NULL;
	argv = pty->argv;
	if (pty->nb_args)
	{
		argv[0] = pty->shell;
		argv[1] = "-c";
		argv[2] = pty->cmd;
		argv[3] = NULL;
	}
	else
		argv = argv_interactive_mode;
	init_shell_pty(pty, file);
	if (execve(pty->shell, argv, env) == ERROR)
		ft_fprintf(STDERR_FILENO, "%s: %s: No such file or directory\n",
								pty->prog_name, pty->shell);
	_exit(1);
}
