/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/07 06:42:09 by garm              #+#    #+#             */
/*   Updated: 2014/10/10 11:56:50 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_script.h"

static char		*get_shell_path(char **env)
{
	int		i;

	i = 0;
	while (env[i] && env[i][0])
	{
		if (ft_strncmp("SHELL=", env[i], 6) == 0)
			return ((env[i][6] != '\0') ? &env[i][6] : NULL);
		i++;
	}
	return (NULL);
}

static void		set_default_opts(t_pty *pty, char **argv, char **env)
{
	char	*shell_path;

	pty->a = 0;
	pty->k = 0;
	pty->q = 0;
	pty->time = 30;
	pty->flush = 0;
	pty->file = "typescript";
	pty->cmd = NULL;
	pty->argv = argv;
	pty->nb_args = 0;
	pty->prog_name = argv[0];
	pty->shell = "";
	if ((shell_path = get_shell_path(env)))
		pty->shell = shell_path;
}

static char		*ft_create_cmd(char **argv, char *cmd, int nb_args)
{
	int		i;

	i = 0;
	while (i < nb_args)
	{
		ft_strcat(cmd, argv[i]);
		if (i < nb_args - 1)
			ft_strcat(cmd, " ");
		i++;
	}
	return (cmd);
}

static void		set_time(int argc, char **argv, t_pty *pty)
{
	int		argc_pos;
	char	cmd[_POSIX_ARG_MAX];

	argc_pos = ft_getopt(GET, THE, ARGC, POSITION);
	if (argc_pos < argc)
		pty->file = argv[argc_pos++];
	if ((argc - argc_pos) >= 0)
		pty->nb_args = argc - argc_pos;
	if (argc_pos < argc)
		pty->cmd = ft_create_cmd(&argv[argc_pos], cmd, pty->nb_args);
}

int				ft_script_parser(int argc, char **argv, char **env, t_pty *pty)
{
	t_opt	optinfo;
	int		opt;

	set_default_opts(pty, argv, env);
	while ((opt = ft_getopt(argc, argv, "t:akq", &optinfo)) != ERROR)
	{
		if (opt == 'a')
			pty->a = 1;
		if (opt == 'k')
			pty->k = 1;
		if (opt == 'q')
			pty->q = 1;
		if (opt == 't')
		{
			if (!(optinfo.arg) || !ft_strisnum(optinfo.arg))
				pty->time = 0;
			if ((pty->time = ft_atoi(optinfo.arg)) < 0)
				return (ERROR);
		}
		if (opt == '?')
			return (optinfo.opt);
	}
	set_time(argc, argv, pty);
	return (FT_SUCCESS);
}
