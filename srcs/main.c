/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/01 06:17:04 by garm              #+#    #+#             */
/*   Updated: 2014/10/10 20:49:13 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_script.h"

int		get_pty(t_pty *pty)
{
	char		*ptsname;
	int			flags;

	flags = O_RDWR;
	if ((pty->master = open_ptm(flags)) == ERROR)
		return (ERROR);
	if (grant_pts(pty->master) == ERROR)
		return (ERROR);
	if (unlock_pts(pty->master) == ERROR)
		return (ERROR);
	if (!(ptsname = get_ptsname(pty->master)))
		return (ERROR);
	if ((pty->slave = open_pts(ptsname, flags)) == ERROR)
		return (ERROR);
	return (0);
}

void	init_term(t_termios *old_conf, t_termios *new_conf, int master)
{
	t_winsize	win;

	if (old_conf && new_conf)
	{
		ioctl(STDIN_FILENO, TIOCGWINSZ, &win);
		ioctl(master, TIOCSWINSZ, &win);
		ioctl(STDIN_FILENO, TIOCGETA, old_conf);
		*new_conf = *old_conf;
		new_conf->c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
		new_conf->c_iflag &= ~(IMAXBEL | IGNBRK | BRKINT | PARMRK | ISTRIP);
		new_conf->c_iflag &= ~(INLCR | IGNCR | ICRNL | IXON);
		new_conf->c_oflag &= ~(OPOST);
		new_conf->c_cflag &= ~(CSIZE | PARENB);
		new_conf->c_cflag |= CS8;
		ioctl(STDIN_FILENO, TIOCSETA, new_conf);
	}
}

void	process(t_pty *pty, int file, char **env)
{
	t_termios	old_conf;
	t_termios	new_conf;

	if (pty->q == 0)
		ft_printf("Script started, output file is %s\n", pty->file);
	init_term(&old_conf, &new_conf, pty->master);
	if (pty->q == 0)
		ft_fprintf(file, "Script started on  %s", get_date());
	pty->fdfile = file;
	pty->last_flush = get_ts();
	ft_script(pty, env, file);
	ioctl(STDIN_FILENO, TIOCSETA, &old_conf);
	if (pty->q == 0)
		ft_printf("\nScript done, output file is %s\n", pty->file);
	if (pty->q == 0)
		ft_fprintf(file, "\nScript done on %s", get_date());
}

int		main(int argc, char **argv, char **env)
{
	t_pty		pty;
	int			file;
	char		bad_opt;

	if ((bad_opt = ft_script_parser(argc, argv, env, &pty)) > FT_SUCCESS)
		return (illegal_option(argv[0], bad_opt));
	if (get_pty(&pty) == ERROR)
		return (pty_error(argv[0]));
	if ((file = open(pty.file, GET_SCRIPT_OPEN_FLAG(pty.a), 0644)) == -1)
		return (permission_denied(argv[0], pty.file));
	if (pty.time < 0)
		return (invalid_flush_time(argv[0], pty.time));
	process(&pty, file, env);
	close(file);
	close(pty.slave);
	close(pty.master);
	return (0);
}
