/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flush.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/10 20:34:24 by garm              #+#    #+#             */
/*   Updated: 2014/10/10 20:36:30 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_script.h"

int		flush_ibuffer(t_pty *pty, char **buf_ptr, char *buffer, int *i)
{
	if (pty->k)
		write(pty->fdfile, buffer, *i);
	*i = 0;
	*buf_ptr = buffer;
	return (0);
}

int		flush_obuffer(t_pty *pty, char **buf_ptr, char *buffer, int *i)
{
	write(pty->fdfile, buffer, *i);
	*i = 0;
	*buf_ptr = buffer;
	return (0);
}

int		try_flush_input(t_pty *pty, char **buf_ptr, char *buffer, int *i)
{
	if (pty->flush && *i)
	{
		flush_ibuffer(pty, buf_ptr, buffer, i);
		return (1);
	}
	else if (pty->flush)
		return (1);
	else
		return (0);
}

int		try_flush_output(t_pty *pty, char **buf_ptr, char *buffer, int *i)
{
	if (pty->flush && *i)
	{
		flush_obuffer(pty, buf_ptr, buffer, i);
		return (1);
	}
	else if (pty->flush)
		return (1);
	else
		return (0);
}
