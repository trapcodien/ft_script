/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   time.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/10 20:37:27 by garm              #+#    #+#             */
/*   Updated: 2014/10/10 20:39:19 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_script.h"

time_t	get_ts(void)
{
	struct timeval		tv;

	if ((gettimeofday(&tv, NULL)) == ERROR)
		return (ERROR);
	return (tv.tv_sec);
}

char	*get_date(void)
{
	struct timeval		tv;

	if ((gettimeofday(&tv, NULL)) == ERROR)
		return (NULL);
	return (ctime(&(tv.tv_sec)));
}
