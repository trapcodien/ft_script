/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   core.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/03 03:22:50 by garm              #+#    #+#             */
/*   Updated: 2014/10/10 20:50:44 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_script.h"

int		write_user_input(t_pty *pty, int master, int file)
{
	static char		input_buffer[BUFF_SIZE];
	static char		*buf_ptr = input_buffer;
	static int		i = 0;
	int				rc;

	if (try_flush_input(pty, &buf_ptr, input_buffer, &i))
		return (0);
	rc = read(STDIN_FILENO, buf_ptr, BUFF_SIZE - i);
	i += rc;
	if (rc < 0)
	{
		write(file, input_buffer, i);
		return (ERROR);
	}
	else if (rc == 0 && (!pty->flush))
		write(master, input_buffer, 0);
	else
	{
		write(master, buf_ptr, rc);
		if (i >= BUFF_SIZE || pty->time == 0)
			flush_ibuffer(pty, &buf_ptr, input_buffer, &i);
		else
			buf_ptr += rc;
	}
	return (0);
}

int		write_shell_output(t_pty *pty, int master, int file)
{
	static char		output_buffer[BUFF_SIZE];
	static char		*buf_ptr = output_buffer;
	static int		i = 0;
	int				rc;

	if (try_flush_output(pty, &buf_ptr, output_buffer, &i))
		return (0);
	rc = read(master, buf_ptr, BUFF_SIZE - i);
	i += rc;
	if (rc <= 0)
	{
		write(file, output_buffer, i);
		return (ERROR);
	}
	write(STDOUT_FILENO, buf_ptr, rc);
	if (i >= BUFF_SIZE || pty->time == 0)
		flush_obuffer(pty, &buf_ptr, output_buffer, &i);
	else
		buf_ptr += rc;
	return (0);
}

int		watch_pty(t_pty *pty, int master, fd_set *fds)
{
	struct timeval	tv;

	tv.tv_sec = pty->time;
	tv.tv_usec = 0;
	FD_ZERO(fds);
	FD_SET(master, fds);
	FD_SET(STDIN_FILENO, fds);
	pty->flush = 0;
	return (select(master + 1, fds, NULL, NULL, &tv));
}

void	ft_script(t_pty *pty, char **env, int file)
{
	fd_set				fds;
	int					n;

	n = 0;
	if (fork())
	{
		close(pty->slave);
		while ((n = watch_pty(pty, pty->master, &fds)) >= 0 || errno == EINTR)
		{
			if (!n || ((get_ts() - pty->last_flush) >= pty->time && pty->time))
			{
				pty->last_flush = get_ts();
				pty->flush = 1;
			}
			if ((pty->flush || (n > 0 && FD_ISSET(pty->master, &fds))) &&
			(write_shell_output(pty, pty->master, file) == ERROR))
				break ;
			if ((pty->flush || (n > 0 && FD_ISSET(STDIN_FILENO, &fds))) &&
			(write_user_input(pty, pty->master, file) == ERROR))
				break ;
		}
	}
	else
		exec_shell(pty, env, file);
}
