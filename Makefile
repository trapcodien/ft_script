# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: garm <garm@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/08/26 23:49:31 by garm              #+#    #+#              #
#    Updated: 2014/10/10 20:37:21 by garm             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc

NAME = ft_script
LIBS = -lft
FTLIBS = libft.a

LIB_DIR = libft
SOURCES_DIR = srcs
INCLUDES_DIR = includes

LIB_DEP = libft/libft.a

ifeq ($(DEBUG), 1)
	FLAGS = -g -Wall -Wextra -pedantic
	CC = cc
else
	FLAGS = -Wall -Werror -Wextra -ansi -pedantic -std=c89
endif

ifeq ($(STRICT), 1)
	FLAGS += -fstack-protector-all -Wshadow -Wunreachable-code \
			  -Wstack-protector -pedantic-errors -O0 -W -Wundef -fno-common \
			  -Wfatal-errors -Wstrict-prototypes -Wmissing-prototypes \
			  -Wwrite-strings -Wunknown-pragmas -Wdeclaration-after-statement \
			  -Wold-style-definition -Wmissing-field-initializers \
			  -Wpointer-arith -Wnested-externs -Wstrict-overflow=5 -fno-common \
			  -Wno-missing-field-initializers -Wswitch-default -Wswitch-enum \
			  -Wbad-function-cast -Wredundant-decls -fno-omit-frame-pointer \
			  -Wfloat-equal
endif

CFLAGS =  $(FLAGS) -I $(INCLUDES_DIR) -I ./$(LIB_DIR)/includes
LDFLAGS = -L $(LIB_DIR) $(LIBS)

DEPENDENCIES = \
			   $(INCLUDES_DIR)/ft_script.h \
			   $(INCLUDES_DIR)/pty.h \
			   \

SOURCES = \
		  $(SOURCES_DIR)/main.c \
		  $(SOURCES_DIR)/error.c \
		  $(SOURCES_DIR)/time.c \
		  $(SOURCES_DIR)/parser.c \
		  $(SOURCES_DIR)/pty.c \
		  $(SOURCES_DIR)/core.c \
		  $(SOURCES_DIR)/flush.c \
		  $(SOURCES_DIR)/shell.c \
		  \

OBJS = $(SOURCES:.c=.o)

all: $(NAME)

%.o: %.c $(DEPENDENCIES)
	$(CC) -c $< -o $@ $(CFLAGS)

$(NAME): $(OBJS) $(LIB_DEP)
	@echo Creating $(NAME)...
	@$(CC) -o $(NAME) $(OBJS) $(LDFLAGS)

$(LIB_DEP):
	@make $(FTLIBS) -C $(LIB_DIR)

test:
	@$(MAKE)
	@echo
	@echo ------- Program Start -------
	@echo
	@./$(NAME)

clean:
	@make clean -C $(LIB_DIR)
	@rm -f $(OBJS)
	@echo Deleting $(NAME) OBJECTS files...

fclean: clean
	@make fclean -C $(LIB_DIR)
	@rm -f $(NAME)
	@echo Deleting $(NAME)...

re: fclean all

.PHONY: clean fclean re all test

