/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdelmultic.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/18 20:52:35 by garm              #+#    #+#             */
/*   Updated: 2014/06/18 22:15:51 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdelmultic(const char *s, char c)
{
	size_t		i;
	size_t		j;
	char		*new_str;
	int			state;

	if (!s || !(new_str = ft_strnew(ft_strlen(s))))
		return (NULL);
	state = 0;
	i = 0;
	j = 0;
	while (s[i])
	{
		if (c == s[i])
			state++;
		else
			state = 0;
		if (state < 2)
		{
			new_str[j] = s[i];
			j++;
		}
		i++;
	}
	return (new_str);
}
