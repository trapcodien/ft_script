/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/17 01:32:58 by garm              #+#    #+#             */
/*   Updated: 2014/09/23 12:12:41 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strjoin(const char *s1, const char *s2)
{
	size_t		len;

	len = ft_strlen(s1) + ft_strlen(s2);
	return (ft_strcat(ft_strcat(ft_strnew(len), s1), s2));
}
