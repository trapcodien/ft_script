/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/17 03:00:22 by garm              #+#    #+#             */
/*   Updated: 2014/09/23 12:15:58 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(const char *s)
{
	int			start;
	int			len;
	int			cutted_end;

	if (!s)
		return (NULL);
	len = ft_strlen(s);
	start = 0;
	while (s && s[start] && ft_iswhite(s[start]))
		start++;
	cutted_end = len - 1;
	while (cutted_end >= 0 && s && ft_iswhite(s[cutted_end]))
		cutted_end--;
	len = cutted_end - start + 1;
	return (ft_strsub(s, start, len));
}
