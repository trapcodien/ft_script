/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnstr_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/17 00:21:36 by garm              #+#    #+#             */
/*   Updated: 2014/06/17 00:23:40 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_putnstr_fd(const char *str, size_t n, int fd)
{
	if (str && *str && n > 0)
	{
		write(fd, str, n);
		return ((char *)str + n);
	}
	return ((char *)str);
}
