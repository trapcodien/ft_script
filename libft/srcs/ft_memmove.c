/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/15 19:39:36 by garm              #+#    #+#             */
/*   Updated: 2014/06/15 19:47:11 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		*ft_memmove(void *s1, const void *s2, size_t n)
{
	size_t		i;
	char		*str1;
	const char	*str2;

	if (!s1 || !s2)
		return (s1);
	if (s1 < s2)
		ft_memcpy(s1, s2, n);
	else
	{
		str1 = (char *)s1;
		str2 = (const char *)s2;
		i = n - 1;
		while ((long)i >= 0)
		{
			str1[i] = str2[i];
			i--;
		}
	}
	return (s1);
}
