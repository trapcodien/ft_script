/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/15 20:29:04 by garm              #+#    #+#             */
/*   Updated: 2014/06/15 23:11:23 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char		*ft_strdup(const char *s)
{
	char	*newstr;
	size_t	len;

	if (!s)
		s = "";
	newstr = NULL;
	len = ft_strlen(s);
	newstr = (char *)malloc((len + 1) * sizeof(char));
	ft_strcpy(newstr, s);
	return (newstr);
}
